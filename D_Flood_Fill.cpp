#include <bits/stdc++.h>

using namespace std;
const int N = 5005;
int n, k, a[N], f[N][N]; //f[i][j]: 前i个全变为颜色j所需的最小代价

int main () {
    cin >> n;
    for (int i = 1; i <= n; i++)    cin >> a[i];
    for (int i = 1; i <= n; i++) {
        if (a[i] == a[k])   continue;
        a[++k] = a[i];
    }
    // for (int i = 1; i <= k; i++)    cout << a[i] << ' ';cout << endl;
    n = k;
    for (int len = 2; len <= n; len++) {
        for (int l = 1; l + len - 1 <= n; l++) {
            int r = l + len - 1;
            if (len > 2 && a[l] == a[r])   f[l][r] = f[l+1][r-1] + 1;
            else    f[l][r] = min (f[l+1][r], f[l][r-1]) + 1;
        }
    }
    cout << f[1][n] << endl;
}

//求把所有块变为同一种颜色所需的最小次数
//一次修改一片
//原转移柿子:不同颜色之间无法转移
//区间dp